import EditProduct from '../pages/EditProduct';
import { Link } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

export default function ProductAdminView({product}) {

  const {name, description, price, stock, isActive, _id} = product;

  const disableLabel = isActive ? "Disable" : "Enable";

  function productDisable(event) {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/disable/${_id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }

  function productEnable(event) {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/enable/${_id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }

  return (
    <Container>
      <Row>
        <Col xs = {2}>{name}</Col>
        <Col xs = {6}>{description}</Col>
        <Col xs = {1}>{price}</Col>
        <Col xs = {1}>{isActive ? "Available" : "Unavailable"}</Col>
        <Col xs = {2}>
        <Link to={`/products/edit/${_id}`}>
            <Button variant="info" type="button" id="editBtn" className="m-1">
              Edit
            </Button>
            </Link>
            { isActive ?
              <Button variant="warning" type="button" id="disableBtn" onClick={productDisable}>
                {disableLabel}
              </Button>
              :
              <Button variant="primary" type="button" id="disableBtn" onClick={productEnable}>
                {disableLabel}
              </Button>
            }
        </Col>
      </Row>
    </Container>
  );
}