import userContext from '../UserContext' 

import {useState, useContext} from 'react';

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {NavLink} from 'react-router-dom';

export default function NavBar() {

  const {user} = useContext(userContext);

  return (
    <Navbar bg="primary" variant="dark">
      <Container>
        <Navbar.Brand href="/">Sari-Sari</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link href="/">Products</Nav.Link>
          </Nav>
          <Nav className="ms-auto">
          { (user.id !== null && user.isAdmin == true) ?
            <>
            <Nav.Link as={NavLink} to="/admin">Dashboard</Nav.Link>
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
            </>
            :(user.id !== null && user.isAdmin !== true) ?
            <>
            <Nav.Link as={NavLink} to="/">Cart</Nav.Link>
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
            </>
            :
            <>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            </>
          }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}