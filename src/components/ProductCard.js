import {Link, NavLink} from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default function ProductCard({product}) {

const {image, name, description, price, stock, _id} = product;

  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={image} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>
          Description: <br/>
          {description}
        </Card.Text>
        <Card.Text>
          PHP: {price}
        </Card.Text>
        <Card.Text>
          Stocks left: {stock}
        </Card.Text>
        <Button variant="primary" as={NavLink} to={`/product/${_id}`}>Check Details</Button>
      </Card.Body>
    </Card>
  );
}