import ProductsCatalog from './pages/ProductsCatalog';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import SpecificProduct from './pages/SpecificProduct';
import AdminDashboard from './pages/AdminDashboard';
import AddProduct from './pages/AddProduct';
import EditProduct from './pages/EditProduct';

import NavBar from './components/NavBar';

import {UserProvider} from './UserContext';

import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

// import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch (`${process.env.REACT_APP_API_URL}/users/detail`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
    }, []);

console.log(user.isAdmin)

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <NavBar/>
            <Container>
              <Routes>
              <Route path="/" element={<ProductsCatalog/>}/>
              <Route path="/products" element={<ProductsCatalog/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="/product/:id" element={<SpecificProduct/>}/>
              <Route path="/admin" element={<AdminDashboard/>}/>
              <Route path="/products/add" element={<AddProduct/>}/>
              <Route path="/products/edit/:id" element={<EditProduct/>}/>
              </Routes>
            </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
