import userContext from '../UserContext' 

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import {Navigate} from 'react-router-dom'

export default function AddProduct() {
  const {user} = useContext(userContext);

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [stock, setStock] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const [imageFile, setImageFile] = useState(null);
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
        if(name !== "" && description !== "" && price !== "" && stock !== ""){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [name, description, price, stock])

  function submitProduct(data) {
    data.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        image: imageUrl,
        name: name,
        description: description,
        price: price,
        stock: stock
      })
    })
    .then(res => res.json())
    .then(data => {
      if( data.addProductSuccess !== false){
          Swal.fire({
            title: "Product add successful",
            icon: "success",
            text: "Product has been successfully added!"
        })
        } else {
            Swal.fire({
              title: "Product add failed",
              icon: "error",
              text: "Please try again."
        })
      }
    })
  }

  const handleImageUrlChange = (e) => {
    setImageUrl(e.target.value);
    setImageFile(null);
  };

  const handleImageFileChange = (e) => {
    setImageUrl('');
    setImageFile(e.target.files[0]);
  };


  return (
  <>
    { (user.isAdmin == true) ?
    <Form onSubmit={(data)=> submitProduct(data)}>
      <Form.Group className="mb-3" controlId="productName">
        <Form.Label>Name:</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter product name" 
        value={name}
        onChange={e => setName(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formImageUrl">
        <Form.Label>Image URL</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter image URL" 
        value={imageUrl} 
        onChange={handleImageUrlChange} />
      </Form.Group>

      <Form.Group className="mb-3" controlId="productDescription">
        <Form.Label>Description:</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter product description" 
        value={description}
        onChange={e => setDescription(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="productPrice">
        <Form.Label>Price:</Form.Label>
        <Form.Control 
        type="number" 
        placeholder="Enter product price" 
        value={price}
        onChange={e => setPrice(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="productPrice">
        <Form.Label>Stock:</Form.Label>
        <Form.Control 
        type="number" 
        placeholder="Enter product price" 
        value={stock}
        onChange={e => setStock(e.target.value)}
        required
        />
      </Form.Group>
      { isActive ?
      <Button variant="primary" type="submit" id="submitBtn">
        Submit
      </Button>
      :
      <Button variant="danger" type="submit" id="submitBtn" disabled>
       Submit
      </Button>
      }
    </Form>
    :
    <Navigate to ="/"/>
    }
  </>
);
}