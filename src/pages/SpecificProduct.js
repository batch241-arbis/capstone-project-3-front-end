import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";

import Swal from 'sweetalert2';

export default function EditProduct() {
  const {id} = useParams();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [stock, setStock] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const [stockHolder, setStockHolder] = useState(0);
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStock(data.stock);
        setImageUrl(data.image);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    if (stock > 0) {
      setIsActive(true);
    } else {
      setIsActive(false);
    };
  });

  const increaseStock = () => {
    if (stockHolder < stock) {
      setStockHolder(stockHolder + 1);
    };
  };

  const decreaseStock = () => {
    if (stockHolder !== 0) {
      setStockHolder(stockHolder - 1);
    };
  };

const checkOut = (event) => {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/checkout/${id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: {id},
        quantity: stockHolder
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
    })
  }

  return (
    <div className="d-flex justify-content-around">
        <Card style={
          { width: '30vw' },
          {height: '10vh'}
        }>
      <Card.Img variant="top" src={imageUrl} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>
          Description: <br/>
          {description}
        </Card.Text>
        <Card.Text>
          PHP: {price}
        </Card.Text>
        <Card.Text>
          Stocks left: {stock}
        </Card.Text>
           <Card.Text className="d-flex align-items-center">
              <Button variant="danger" onClick={decreaseStock} className="m-2">-</Button>
              {stockHolder}
              <Button variant="success" onClick={increaseStock} className="m-2">+</Button>
            </Card.Text>
            {isActive?
            <Button variant="success" onClick={checkOut} className="m-1">Checkout</Button>
            :
            <Button variant="success" onClick={checkOut} className="m-1" disable>Checkout</Button>
            }
      </Card.Body>
    </Card>
    </div>
  );
}