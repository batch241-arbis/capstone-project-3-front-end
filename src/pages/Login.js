import { Form, Button, Container } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import {useNavigate, Navigate} from 'react-router-dom';

import userContext from '../UserContext'

export default function Login() {

    const navigate = useNavigate();

    const {user, setUser} = useContext(userContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function loginUser(data) {
        data.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if( data.loginSuccess !== false){
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);
                    Swal.fire({
                        title: "Login successful",
                        icon: "success",
                        text: "Welcome Back!"
                })
                    navigate("/");
                } else {
                    Swal.fire({
                        title: "Login Fail",
                        icon: "error",
                        text: "Please check your credentials."
                })
            }
        })
    }

    useEffect(() => {
        if(((password !== "") && (password.length >= 8)) && (email !== "")){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password])

    const retrieveUserDetails = (token) => {
  fetch(`https://capstone-project-3.onrender.com/users/detail`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
    .then((res) => res.json())
    .then((data) => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};
	return (
        (user.id !== null) ?
        <Navigate to ="/"/>
        :
        <Container className="mt-3">
        <h1 className="d-flex justify-content-center mb-3">
        Login
        </h1>
        <Form onSubmit={(data)=> loginUser(data)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email:</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter your login email" 
	                value={email}
	                onChange={e => setEmail(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password:</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password}
	                onChange={e => setPassword(e.target.value)}
	                required
                />
            </Form.Group>

            { isActive ?
            <Button variant="primary" type="submit" id="submitBtn" className="mt-3">
                Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn" className="mt-3" disabled>
                Submit
            </Button>
            }
        </Form>
        </Container>
    )
}