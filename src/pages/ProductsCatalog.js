import ProductCard from '../components/ProductCard';

import {useState, useEffect} from 'react'
import { Container, Row, Col } from 'react-bootstrap';

export default function Products(){
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/forsale`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} product={product}/>
				)
			}))

		})
	}, [])

	return (
			<>
			<Container className="d-flex m-3 justify-content-center">
      			<Row>
        			{products.map((product, index) => (
          				<Col key={index} md={4} sm={8} className="mb-3">
            				{product}
          				</Col>
        			))}
      			</Row>
    		</Container>
			</>
			)
}