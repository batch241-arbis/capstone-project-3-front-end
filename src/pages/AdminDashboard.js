import userContext from '../UserContext' 

import ProductAdminView from '../components/ProductAdminView';
import {useState, useEffect, useContext} from 'react';
import {Link, Navigate} from 'react-router-dom';
import { Container, Button, Row, Col } from 'react-bootstrap';

export default function AdminDashboard() {
  const {user} = useContext(userContext);

	const [products, setProducts] = useState([]);

	useEffect(() => {
    const interval = setInterval(() => {
      fetch(`${process.env.REACT_APP_API_URL}/products/all`)
        .then(res => res.json())
        .then(data => {
        	console.log("here")
          setProducts(data.map(product => {
            return (
              <ProductAdminView key={product._id} product={product} />
            )
          }))
        })
    }, 5000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/products/all`)
        .then(res => res.json())
        .then(data => {
          console.log("here")
          setProducts(data.map(product => {
            return (
              <ProductAdminView key={product._id} product={product} />
            )
          }))
        })
    }, []);

	const AdminBanner = () => {
		return (
      <>
		<div className="mt-5 d-flex justify-content-center">
            <h1>
                Admin Dashboard
            </h1>
            </div>
            <div className="mt-2 d-flex justify-content-center">
                <Row>
                    <Col><Button className="bg-primary" as={Link} to="/products/add">Add Product</Button></Col>
                    <Col><Button className="bg-primary" as={Link} to="/products">Show User</Button></Col>
                </Row>
            </div>
            </>
        )
	}

	const ProductTableHeader =() => {
		return (
			<Container>
      			<Row>
        			<Col xs = {2}>Name</Col>
        			<Col xs = {6}>Description</Col>
        			<Col xs = {1}>Price</Col>
        			<Col xs = {1}>Availability</Col>
        			<Col xs = {2}>Actions</Col>
      			</Row>
    		</Container>
		)
	}

	return (
    user.isAdmin === true ? (
      <>
        <AdminBanner/>
        <ProductTableHeader/>
        {products}
      </>
    ) : (
      <Navigate to="/"/>
    )
  );
}