import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import {useNavigate, Navigate, Link} from 'react-router-dom';
import Container from 'react-bootstrap/Container';

import userContext from '../UserContext'

export default function Register() {

	const navigate = useNavigate();

	const {user, setUser} = useContext(userContext);

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(data) {
		data.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			if( data.registerSuccess !== false){
					Swal.fire({
						title: "Registration successful",
	 					icon: "success",
	 					text: "Thank you for registering!"
				})
					navigate("/login");
				} else {
	  				Swal.fire({
	  					title: "Duplicate email found",
	  					icon: "error",
	  					text: "Please provide a different email."
				})
			}
		})
	}

	useEffect(() => {
		if(((password1 === password2) && (password1 !== "") && (password1.length >= 8)) && (email !== "")){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2])

	return (
		(user.id !== null) ?
        <Navigate to ="/"/>
        :
		<Container className="mt-3">
		<h1 className="d-flex justify-content-center mb-3">
			Register
		</h1>
        <Form onSubmit={(data)=> registerUser(data)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email:</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter your email" 
	                value={email}
	                onChange={e => setEmail(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password:</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password1}
	                onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password:</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
	                value={password2}
	                onChange={e => setPassword2(e.target.value)}
	                required
                />
            </Form.Group>

            { isActive ?
            <Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-1">
            	Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-1" disabled>
            	Submit
            </Button>
        	}

        	<p className="mx-auto">
  Already have an account? <Link to="/login">Click here</Link> to log in.
</p>
        </Form>
        </Container>
    )
}