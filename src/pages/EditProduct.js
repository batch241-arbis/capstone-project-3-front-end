import userContext from '../UserContext' 

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';
import { useParams, Navigate } from "react-router-dom";

export default function EditProduct() {
  const {user} = useContext(userContext);

  const {id} = useParams();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [stock, setStock] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStock(data.stock);
        setImageUrl(data.image);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    if (name !== '' && description !== '' && price !== '' && stock !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price, stock]);

  function editProduct(event) {
    event.preventDefault();

    const formData = new FormData();
    formData.append('name', name);
    formData.append('description', description);
    formData.append('price', price);
    formData.append('stock', stock);

    fetch(`${process.env.REACT_APP_API_URL}/products/edit/${id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        image: imageUrl,
        name: name,
        description: description,
        price: price,
        stock: stock
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data.editProductSuccess)
        if (data.editProductSuccess == true) {
          Swal.fire({
            title: 'Product edit successful',
            icon: 'success',
            text: 'Product has been successfully edited!',
          });
        } else {
          Swal.fire({
            title: 'Product edit failed',
            icon: 'error',
            text: 'Please try again.',
          });
        }
      })
  }

  const handleImageUrlChange = (event) => {
    setImageUrl(event.target.value);
  };

  return (
    user.isAdmin === true ? (
      <>
    <Form onSubmit={(data)=> editProduct(data)}>
      <Form.Group className="mb-3" controlId="productName">
        <Form.Label>Name:</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter product name" 
        value={name}
        onChange={e => setName(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formImageUrl">
        <Form.Label>Image URL</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter image URL" 
        value={imageUrl} 
        onChange={handleImageUrlChange} />
      </Form.Group>

      <Form.Group className="mb-3" controlId="productDescription">
        <Form.Label>Description:</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter product description" 
        value={description}
        onChange={e => setDescription(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="productPrice">
        <Form.Label>Price:</Form.Label>
        <Form.Control 
        type="number" 
        placeholder="Enter product price" 
        value={price}
        onChange={e => setPrice(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="productPrice">
        <Form.Label>Stock:</Form.Label>
        <Form.Control 
        type="number" 
        placeholder="Enter product price" 
        value={stock}
        onChange={e => setStock(e.target.value)}
        required
        />
      </Form.Group>
      { isActive ?
      <Button variant="primary" type="submit" id="submitBtn">
        Submit
      </Button>
      :
      <Button variant="danger" type="submit" id="submitBtn" disabled>
       Submit
      </Button>
      }
    </Form>
    </>
    ) : (
      <Navigate to="/"/>
    )
  );
}